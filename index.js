var express= require('express')
var expressBodyParser= require('body-parser')

var app= express() // экземпляр приложения



app.use(express.static(__dirname+'/public'))

// GET http://localhost:8000/hello
app.get('/hello', function (req, res, next) { // обработчик запроса
  res.send('hello world')
})



// API блога, например

var blog= {
    posts: [
        {}, {}, {}, {}, {}, {}, {}
    ]
}

// список постов
app.get('/blog/posts', function (req, res, next) {
    res.send(blog.posts)
})

// добавить пост в список
app.post('/blog/posts', expressBodyParser.json(), function (req, res, next) {
    var post= req.body
    console.log('post', post)
    blog.posts.push(post)
    res.status(200).send(post)
})



var port= 8000 // http://localhost:8000

app.listen(port, function () {
    console.log('application listening on port %d', port)
})
